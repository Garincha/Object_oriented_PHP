<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of static_properties_method
 *
 * @author john
 */
class static_properties_method {
    public static $name = "Frank";
    public static $age = "49";
    
    public function display(){
        echo "Age is ".self::$age;
        echo '<br>';
    }
    
    public static function name(){
        echo "Name is ".self::$name;
        echo '<br>';
    }
}
$obj = new static_properties_method();


static_properties_method::name();//calling a static method
$obj->display();//calling a method with static property