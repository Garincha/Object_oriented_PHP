<?php

interface School{
    public function mySchool();
} 

interface college{
    public function mycollege();
}

interface university{
    public function myuniversity();
}


class Teacher implements School, college, university{//here we are including all interface in one class
    
    public function __construct() {
        $this->mySchool();
        $this->mycollege();
        $this->myuniversity();
    }

    public function mySchool() {
        echo "I am a school teacher.<br>";;
    }
    public function mycollege() {
        echo "I am a College Teacher.<br>";
    }
    public function myuniversity(){
        echo "I am a University Teacher.";
    }
}

$edu = new Teacher();


