<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of prevent_extending_overiding
 *
 * @author john
 */
final class prevent_extending_overiding {//here the final word prevent us to make any sub class of this class(preventing inheritance).
    public $user = "";
    public $userid = "";
    
    public function __construct($a,$b) {
        $this->user = $a;
        $this->userid = $b;
    }
    
    public final function display(){//here the final word prevent us to overwright this method.
        echo "User name is {$this->user} and ID is {$this->userid}";
        echo "<br>";
    }
}
class extra extends prevent_extending_overiding{
    public $admin = "";
    
    public function display() {
        echo "User name is {$this->user} ans Id is {$this->userid} and level is {$this->admin}";
    }
}
$obj = new inheritance("Sam","234");
$obj->display();
$adm = new admin("Alex","1");
$adm->admin = "Administrator";
$adm->display();