<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of magic_methods
 *
 * @author john
 */
class magic_methods {
    public $name;


    public function student(){
        echo 'I am a student.<br>';
    }
    
    public function __get($pm) {
        echo "$pm does not exist<br>";;
    }
    
    public function __set($pm, $value) {
        echo "We set two values $pm->$value"."<br>";
    }
    
    public function __call($pm, $value) {// parameters value stores in $value
        echo "There is no <b>$pm</b> methods and aruments are :".  implode(' ,', $value);// parameters value stores in $value
    }
}

$str = new magic_methods();
$str->student();
$str->Delon;//Delon is a undefined property, by get method we are printing this.
$str->age=15;//age is a undefined property with value, by set method we are printin this property.
$str->notexistMethod('3','6','9');//this is a undefined method, by call methods we are printing this methods value.