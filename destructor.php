<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of destructor
 *
 * @author john
 */
class destructor {
    public $name = "";
    public $age = "";
    public $id = "";

    public function __construct($a,$b){
        $this->name = $a;//assigning value in property name.
        $this->age = $b;//assigning value in property age.
    }
    public function PersonDetails(){
        echo "Persons name is {$this->name} and person age is {$this->age}";
    }
    public function setId($c){
        $this->id = $c;
    }
    
    public function __destruct() {
        if(!empty($this->id)){
            echo 'Saving Person';
        }
    }
}

$obj = new destructor("Shalika","34");
$obj->setId(12);
unset($obj);