<?php

spl_autoload_register(function ($class_name){
    include $class_name.".php";
    });
    
    class pkpChild extends pkp{
        public function bestFramework(){
        echo "Child class constant and class name ".__CLASS__.'<br>';//__CLASS__ this keyword returns the name of the class.
        echo "Child class function and class name ".  get_class($this)."<br>";//get_class function will return the class name also.
    }
    
    public function ourMethod(){
        parent::bestCMS();//accesssing the public method bestCMS from parent class by scope resolatiob operator parent.
    }
    }
    
    $pkp = new pkpChild();
    $pkp->bestFramework();
    echo '<hr>';
    $pkp->bestCMS();
    echo '<hr>';
    $pkp->ourMethod();
    