<?php

interface School{
    public function mySchool();
}            
class Teacher implements School{
    
    public function __construct() {
        $this->mySchool();
    }

    public function mySchool() {
        echo "I am a school teacher.";;
    }
}

$edu = new Teacher();
