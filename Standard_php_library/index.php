<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

spl_autoload_register(function($class_name){
    include $class_name.".php";
    });
    
$arr = array("HTML","CSS","JavaScript","PHP","Java");
$jan = new ArrayIterator($arr);
echo $jan->current()."<br>";
$jan->next();
echo $jan->current()."<br>";
foreach ($jan as $value) {
    echo $value."<br>";
}

$arr2 = array("one","two","three","four","five");
$sms = new ArrayIterator($arr2);
$kms = new LimitIterator($sms,0,2);//limiting the iteration between the index of array o and 2.
foreach ($kms as $value){
    echo $value."<br>";
}

$arr3 = array("USA","UK","USSR","UN");
$kkm = new ArrayIterator($arr3);
$mmn = new CachingIterator($kkm);
foreach ($mmn as $value) {
    echo $value;
    if($mmn->hasNext()){
        echo ', ';
    }
}