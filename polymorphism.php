<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of polymorphism
 *
 * @author john
 */
class polymorphism {
    public $user = "";
    public $userid = "";
    
    public function __construct($a,$b) {
        $this->user = $a;
        $this->userid = $b;
    }
    
    public function display(){
        echo "User name is {$this->user} and ID is {$this->userid}";
        echo "<br>";
    }
}
class admin extends polymorphism{
    public $admin = "";//here we are adding a new property.this is called polymorphism.
    
    public function display() {
        echo "User name is {$this->user} ans Id is {$this->userid} and level is {$this->admin}";//here we are overridiing the method of main class, this is a part of polymorphism.
    }
    
}
$obj = new polymorphism("Sam","234");
$obj->display();
$adm = new admin("Alex","1");
$adm->admin = "Administrator";
$adm->display();
echo '<br>';
if($adm instanceof polymorphism){
    echo "inheritated";//if this prints, then polymorphism is working.
}  else {
    echo "Not properly inheritated" ;
}
