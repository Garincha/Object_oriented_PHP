<?php
//include_once './autoloading_class/Java.php';
//include_once './autoloading_class/PHP.php';
//include_once './autoloading_class/Ruby.php';

//function __autoload($class_name){//using this function to load all classes at a time.
    //include './autoloading_class/'.$class_name.'.php';
//}

spl_autoload_register(function($class_name){//this is latest function of autoloading
    include './autoloading_class/'.$class_name.'.php';
});

$ruby = new Ruby();
$java = new Java();
$php = new PHP();
