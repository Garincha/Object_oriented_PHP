<?php

abstract class student{// by the keyword abstract, this class becomes abstract class.
    public $name = "";
    public $age = "";
    
    public function details(){//this is a non abstract method.
       echo $this->name." is ".$this->age." years old.<br>";
    }
    
    abstract public function school();//this is a abstract method.before making an abstract method the class must be declared abstract. 
        
    
}

class extra extends student{
    public function repeat(){
        return parent::details()."And i am a Phd student.<br>";
    }
    
    public function school() {
        return "We are overriding the value of abstract method.";
    }
}




//$obj = new student();//from abstract class,we can't make an object directly.
$obj = new extra();//after making a sub class of abstract student class,now we are making a object of that sub class.
$obj->name="Honaldo";
$obj->age="35";
echo $obj->repeat();//we are echoing this method,cause we retured value inside that method.
echo $obj->school();

