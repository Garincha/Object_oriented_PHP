<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of contruct_&_destruct
 *
 * @author john
 */
class contruct_destruct {
    public $user = "";
    public $userid = "";
    
    public function __construct($a,$b) {
        $this->user = $a;
        $this->userid = $b;
        echo "User name is {$this->user} and user id is {$this->userid}";
    }
    public function __destruct() {
        unset($this->user);
        unset($this->userid);
    }
}
$obj = new contruct_destruct('Herbert','49');
