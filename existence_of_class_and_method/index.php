<?php

include_once './student.php';

if(class_exists("student")){
    $str = new student();//if the class exists, the below code will run.
    if(method_exists($str,'details')){//if the method exists, the below code will run.
        $str->details();
    }  else {
        echo 'Methods not found';
    }
}else{
    echo 'Class is not found';
}