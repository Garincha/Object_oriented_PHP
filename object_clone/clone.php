<?php

spl_autoload_register(function ($class_name){
    include $class_name.".php";
    });
    
$jec = new language;
$jec->setCategory("OOP");
$jec->setFramework("Laravel");
echo $jec->getCategory()."<br>";
echo $jec->getFramework()."<br>";

$sam = clone $jec;
echo $sam->getCategory()."<br>";
echo $sam->getFramework()."<br>";
$sam->setCategory("structural programming");
$sam->setFramework("no framework");
echo $sam->getCategory()."<br>";
echo $sam->getFramework();
