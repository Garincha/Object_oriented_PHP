<?php

spl_autoload_register(function ($class_name){
    include $class_name.".php";
    });
    
$jec = new language;
$jec->setCategory("OOP");
$jec->setFramework("Laravel");
echo $jec->getCategory()."<br>";
echo $jec->getFramework()."<br>";

$php = $jec;//copying references of $jec to $php,not value.
$php->setFramework("cakePHP");//setting the new value.

echo $jec->getCategory()."<br>";
echo $jec->getFramework()."<br>";

echo $php->getCategory()."<br>";
echo $php->getFramework()."<br>";