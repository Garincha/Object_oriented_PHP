<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of access_modifier
 *
 * @author john
 */
class access_modifier {
    public $name = "Zico";
    private $age = "38";
    protected $profession = "PHP Developer";
    
    public function show(){
        echo $this->name." this is from a public access modifier(main class).<br>";
        echo "$this->age"." this is from a private access modifier(main class).<br>";
        echo "$this->profession"." this is from a protected access modifier(main class).<br>";
    }
 }
 class sub extends access_modifier{
     public function display(){
         echo $this->name." this is from a public access modifier(sub class)";
         echo "$this->age"." this is from a private access modifier(sub class).<br>";
         echo "$this->profession"." this is from a protected access modifier(sub class).<br>";
     }
 }
 $obj = new access_modifier();
 $obj->show();
 $obj2 = new sub;
 $obj2->display();
 echo $obj->name." from outside of main class";
 