<?php include_once './calculator.php';?>



<form action="" method="post">
    <table>
        <tr>
            <td>Enter Your First Number: </td>
            <td><input type="number" name="num1"></td>
        </tr>
        <tr>
            <td>Enter Your Second Number: </td>
            <td><input type="number" name="num2"></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="cal" value="Calculator"</td>
        </tr>
    </table>
</form>

<?php
    if(isset($_POST['cal']) && !empty($_POST['cal'])){
        $numone = $_POST['num1'];
        $numtwo = $_POST['num2'];
        if (empty($numone) or empty($numtwo)) {
            echo "<span style='color:red'>Fields must not be empty.</span>";
        }  else {
            echo "First Number is :".$numone."<br>";
            echo "Second Number is :".$numtwo."<br>";
            $cal = new calculator();
            $cal->add($numone,$numtwo);
            $cal->sub($numone,$numtwo);
            $cal->multi($numone,$numtwo);
            $cal->div($numone,$numtwo); 
        }
        
    }
    
    
?>