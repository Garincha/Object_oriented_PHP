<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Calculation
 *
 * @author john
 */
class Calculation {
    public $one = 0;
    public $two = 0;
    public $result ;
    
    public function getValue($x,$y){
        $this->one = $x;
        $this->two = $y;
        return $this;
    }
    
    public function getResult(){
        $this->result = $this->one * $this->two;
        return $this->result;
    }
}
