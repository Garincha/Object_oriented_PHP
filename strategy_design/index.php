<?php

spl_autoload_register(function($class_name){
    include $class_name.".php";
    });


$user = new User();
$msg = $user->getMsg();
switch ($msg) {
    case 'email':
        $obj = new SendEmail();
        break;
    case 'sms':
        $obj = new SendSms();
        break;
    case 'fax':
        $obj = new SendFax();
        break;
    default:
        break;
}
$obj->notification();