<?php

spl_autoload_register(function ($class_name){
    include $class_name.".php";
    });
    
$jec = new copy();
$jec->setCategory("OOP");
$jec->setFramework("Laravel");
echo $jec->getCategory()."<br>";
echo $jec->getFramework()."<br>";

$sam = clone $jec;//by clone keyword, we can clone only partially, not deeply.
echo $sam->getCategory()."<br>";
echo $sam->getFramework()."<br>";
$sam->setCategory("structural programming");
$sam->setFramework("no framework");
echo $sam->getCategory()."<br>";
echo $sam->getFramework();

