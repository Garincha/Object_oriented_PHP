<?php

trait Java {
public function javacoder() {
    return "Java is a leading Language.";
}

}
trait Php {
public function phpcoder() {
    return "PHP is a leading Language.";
}
}
class coderOne{
    use Java;//by use keyword, we are applying trait Java here
}

class coderTwo{
    use Php;//by use keyword, we are applying trait Php here
}

class coderThree{
    use Java,Php;//using two traits together
}
$obj = new coderOne();
echo $obj->javacoder()."<br>";
$obj2 = new coderTwo();
echo $obj2->phpcoder()."<br>";
$obj3 = new coderThree();
echo $obj3->javacoder();
echo $obj3->phpcoder();

